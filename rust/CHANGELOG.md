# 0.5.0 (2023-12-03)

- **[Breaking change]** Require Rust 1.70.
- **[Fix]** Avoid pinned `wasm-bindgen` version.
- **[Fix]** Update dependencies.

# 0.4.2 (2021-10-28)

- **[Fix]** Update dependencies.

# 0.4.1 (2021-10-26)

- **[Feature]** Accept any `Root` implementation for `emit_html`.

# 0.4.0 (2021-10-26)

- **[Feature]** Implement emitter support.

# 0.3.0 (2021-10-18)

- **[Feature]** Release to `crates.io`.

# 0.2.0 (2020-09-30)

- **[Breaking change]** Require grammar config for the parser
- **[Feature]** Add support for icons
- **[Feature]** Add support for links
- **[Feature]** Add support for maximum nesting depth limits
- **[Feature]** Add support for `admin` and `mod` blocks
- **[Feature]** Add support for escape sequences

# 0.1.0 (2020-06-18)

- **[Feature]** First release
