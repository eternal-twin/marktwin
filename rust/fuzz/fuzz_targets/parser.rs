#![no_main]
use libfuzzer_sys::fuzz_target;
use marktwin::grammar::Grammar;

fuzz_target!(|data: String| {
   let _ = marktwin::parser::parse(&Grammar::full(), &data);
});
