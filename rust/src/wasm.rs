use serde::Serialize;
use wasm_bindgen::prelude::*;

use crate::grammar::Grammar;
use crate::parser::parse_owned_ast;

/// Parse a Marktwin input
#[wasm_bindgen(js_name = parse)]
pub fn parse(grammar: JsValue, input: String) -> JsValue {
  let grammar: Grammar = serde_wasm_bindgen::from_value(grammar).unwrap();
  let serializer = serde_wasm_bindgen::Serializer::json_compatible();
  parse_owned_ast(&grammar, &input).serialize(&serializer).unwrap()
}

/// Emit HTML for the provided AST
#[wasm_bindgen(js_name = emitHtml)]
pub fn emit_html(marktwin: JsValue) -> String {
  let root: crate::ast::owned::Root = serde_wasm_bindgen::from_value(marktwin).unwrap();
  let mut out: Vec<u8> = Vec::new();
  crate::emitter::emit_html(&mut out, &root).unwrap();
  String::from_utf8(out).unwrap()
}
