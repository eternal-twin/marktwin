use crate::ast::traits;
use crate::ast::traits::{BlockCast, InlineCast};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash)]
pub enum OwnedSyntax {}

impl traits::Syntax for OwnedSyntax {
  type Root = Root;

  type Block = Block;
  type Admin = Admin;
  type Mod = Mod;

  type Inline = Inline;
  type Emphasis = Emphasis;
  type Icon = Icon;
  type Link = Link;
  type Newline = Newline;
  type Strikethrough = Strikethrough;
  type Strong = Strong;
  type Text = Text;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Root {
  pub loc: (),
  pub children: Vec<Block>,
}

impl traits::BlockContainer for Root {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Block>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Root for Root {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Block {
  Admin(Admin),
  Mod(Mod),
  Emphasis(Emphasis),
  Icon(Icon),
  Link(Link),
  Newline(Newline),
  Strikethrough(Strikethrough),
  Strong(Strong),
  Text(Text),
}

impl traits::Block for Block {
  type Ast = OwnedSyntax;

  fn cast(&self) -> BlockCast<OwnedSyntax> {
    match self {
      Block::Admin(ref s) => BlockCast::Admin(traits::MaybeOwned::Borrowed(s)),
      Block::Mod(ref s) => BlockCast::Mod(traits::MaybeOwned::Borrowed(s)),
      Block::Emphasis(ref s) => BlockCast::Emphasis(traits::MaybeOwned::Borrowed(s)),
      Block::Icon(ref s) => BlockCast::Icon(traits::MaybeOwned::Borrowed(s)),
      Block::Link(ref s) => BlockCast::Link(traits::MaybeOwned::Borrowed(s)),
      Block::Newline(ref s) => BlockCast::Newline(traits::MaybeOwned::Borrowed(s)),
      Block::Strikethrough(ref s) => BlockCast::Strikethrough(traits::MaybeOwned::Borrowed(s)),
      Block::Strong(ref s) => BlockCast::Strong(traits::MaybeOwned::Borrowed(s)),
      Block::Text(ref s) => BlockCast::Text(traits::MaybeOwned::Borrowed(s)),
    }
  }
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Admin {
  pub loc: (),
  pub children: Vec<Block>,
}

impl traits::BlockContainer for Admin {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Block>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Admin for Admin {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Mod {
  pub loc: (),
  pub children: Vec<Block>,
}

impl traits::BlockContainer for Mod {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Block>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Mod for Mod {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Inline {
  Emphasis(Emphasis),
  Icon(Icon),
  Link(Link),
  Newline(Newline),
  Strikethrough(Strikethrough),
  Strong(Strong),
  Text(Text),
}

impl traits::Inline for Inline {
  type Ast = OwnedSyntax;

  fn cast(&self) -> InlineCast<OwnedSyntax> {
    match self {
      Inline::Emphasis(ref s) => InlineCast::Emphasis(traits::MaybeOwned::Borrowed(s)),
      Inline::Icon(ref s) => InlineCast::Icon(traits::MaybeOwned::Borrowed(s)),
      Inline::Link(ref s) => InlineCast::Link(traits::MaybeOwned::Borrowed(s)),
      Inline::Newline(ref s) => InlineCast::Newline(traits::MaybeOwned::Borrowed(s)),
      Inline::Strikethrough(ref s) => InlineCast::Strikethrough(traits::MaybeOwned::Borrowed(s)),
      Inline::Strong(ref s) => InlineCast::Strong(traits::MaybeOwned::Borrowed(s)),
      Inline::Text(ref s) => InlineCast::Text(traits::MaybeOwned::Borrowed(s)),
    }
  }
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Emphasis {
  pub loc: (),
  pub children: Vec<Inline>,
}

impl traits::InlineContainer for Emphasis {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Inline>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Emphasis for Emphasis {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Icon {
  pub loc: (),
  pub key: String,
}

impl traits::Icon for Icon {
  type Ast = OwnedSyntax;

  fn key(&self) -> Cow<str> {
    Cow::Borrowed(&self.key)
  }
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Link {
  pub loc: (),
  pub children: Vec<Inline>,
  pub uri: String,
}

impl traits::InlineContainer for Link {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Inline>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Link for Link {
  type Ast = OwnedSyntax;

  fn uri(&self) -> Cow<str> {
    Cow::Borrowed(&self.uri)
  }
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Strikethrough {
  pub loc: (),
  pub children: Vec<Inline>,
}

impl traits::InlineContainer for Strikethrough {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Inline>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Strikethrough for Strikethrough {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Strong {
  pub loc: (),
  pub children: Vec<Inline>,
}

impl traits::InlineContainer for Strong {
  type Ast = OwnedSyntax;

  fn children<'a>(&'a self) -> Box<dyn Iterator<Item = traits::MaybeOwned<'a, Inline>> + 'a> {
    Box::new(self.children.iter().map(traits::MaybeOwned::Borrowed))
  }
}

impl traits::Strong for Strong {
  type Ast = OwnedSyntax;
}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Newline {
  pub loc: (),
}

impl traits::Newline for Newline {}

#[derive(Debug, Eq, PartialEq, Clone, Ord, PartialOrd, Hash, Deserialize, Serialize)]
pub struct Text {
  pub loc: (),
  pub text: String,
}

impl traits::Text for Text {
  type Ast = OwnedSyntax;

  fn text(&self) -> Cow<str> {
    Cow::Borrowed(&self.text)
  }
}
