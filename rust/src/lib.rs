#[macro_use]
mod token_set;

pub mod ast;
pub mod emitter;
pub mod grammar;
pub mod lexer;
pub mod parser;
pub mod syntax;

#[cfg(target_arch = "wasm32")]
mod wasm;
