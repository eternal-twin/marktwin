export enum NodeType {
  /**
   * Message root
   */
  Root = "Root",

  /**
   * Administrator block
   */
  Admin = "Admin",

  /**
   * Moderator block
   */
  Mod = "Mod",

  /**
   * Emphasis span
   */
  Emphasis = "Emphasis",

  /**
   * Icon
   */
  Icon = "Icon",

  /**
   * Link
   */
  Link = "Link",

  /**
   * New line tag
   */
  Newline = "Newline",

  /**
   * Role-play block
   */
  Rp = "Rp",

  /**
   * Strikethrough span
   */
  Strikethrough = "Strikethrough",

  /**
   * Strong span
   */
  Strong = "Strong",

  /**
   * Raw text
   */
  Text = "Text",

  /**
   * User mention
   */
  User = "User",
}

export interface Root {
  type?: NodeType.Root;
  loc: null;
  children: Block[];
}

export interface Admin {
  type: NodeType.Admin;
  loc: null;
  children: Block[];
}

export interface Mod {
  type: NodeType.Mod;
  loc: null;
  children: Block[];
}

export interface Rp {
  type: NodeType.Rp;
  author: Inline | null;
}

export interface Emphasis {
  type: NodeType.Emphasis;
  children: Inline[];
}

export interface Icon {
  type: NodeType.Icon;
  key: string;
}

export interface Link {
  type: NodeType.Link;
  children: Inline[];
  uri: string;
}

export interface Newline {
  type: NodeType.Newline;
}

export interface Strikethrough {
  type: NodeType.Strikethrough;
  children: Inline[];
}

export interface Strong {
  type: NodeType.Strong;
  children: Inline[];
}

export interface Text {
  type: NodeType.Text;
  loc: null;
  text: string;
}

/**
 * User mention
 */
export interface User {
  type: NodeType.User;
  ref: string;
}

/**
 * Inline content
 */
export type Block = Admin | Mod | Rp | Inline;

/**
 * Inline content
 */
export type Inline = Emphasis | Icon | Link | Newline | Strikethrough | Strong | Text | User;

export type BlockContainer = InlineContainer | Root | Admin | Mod;

export type InlineContainer = Emphasis | Link | Strikethrough | Strong;
