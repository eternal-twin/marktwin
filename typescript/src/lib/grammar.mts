export interface Grammar {
  admin: boolean;
  depth: null | number;
  emphasis: boolean;
  icons: string[];
  links: string[];
  mod: boolean;
  quote: boolean;
  spoiler: boolean;
  strong: boolean;
  strikethrough: boolean;
}
