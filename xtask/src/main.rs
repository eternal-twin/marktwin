use clap::Parser;
use std::path::PathBuf;
use wasm_bindgen_cli_support::Bindgen;

/// Arguments to the `precompile` task.
#[derive(Debug, Parser)]
pub struct Args {
  #[arg(long = "out-dir")]
  out_dir: PathBuf,
  #[arg(long = "out-name")]
  out_name: String,
  input: PathBuf,
}

fn main() {
  // Local wasm-bindgen version
  // https://github.com/rustwasm/wasm-bindgen/issues/2702#issuecomment-1240544295
  let args = Args::parse();
  Bindgen::new()
    .input_path(args.input)
    .out_name(args.out_name.as_str())
    .typescript(true)
    .generate(args.out_dir)
    .expect("bindgen error");
}
