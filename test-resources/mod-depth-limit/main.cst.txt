NodeContent@0..86 {
  NodeText@0..2 {
    TokenText@0..2 "a "
  }
  NodeMod@2..75 {
    NodeModOpen@2..7 {
      TokenOpenBracket@2..3 "["
      TokenMod@3..6 "mod"
      TokenCloseBracket@6..7 "]"
    }
    NodeText@7..10 {
      TokenText@7..10 " a "
    }
    NodeMod@10..66 {
      NodeModOpen@10..15 {
        TokenOpenBracket@10..11 "["
        TokenMod@11..14 "mod"
        TokenCloseBracket@14..15 "]"
      }
      NodeText@15..18 {
        TokenText@15..18 " a "
      }
      NodeMod@18..57 {
        NodeModOpen@18..23 {
          TokenOpenBracket@18..19 "["
          TokenMod@19..22 "mod"
          TokenCloseBracket@22..23 "]"
        }
        NodeText@23..26 {
          TokenText@23..26 " a "
        }
        NodeMod@26..48 {
          NodeModOpen@26..31 {
            TokenOpenBracket@26..27 "["
            TokenMod@27..30 "mod"
            TokenCloseBracket@30..31 "]"
          }
          NodeText@31..42 {
            TokenText@31..42 " a [mod] a "
          }
          NodeModClose@42..48 {
            TokenOpenBracket@42..43 "["
            TokenSlash@43..44 "/"
            TokenMod@44..47 "mod"
            TokenCloseBracket@47..48 "]"
          }
        }
        NodeText@48..51 {
          TokenText@48..51 " a "
        }
        NodeModClose@51..57 {
          TokenOpenBracket@51..52 "["
          TokenSlash@52..53 "/"
          TokenMod@53..56 "mod"
          TokenCloseBracket@56..57 "]"
        }
      }
      NodeText@57..60 {
        TokenText@57..60 " a "
      }
      NodeModClose@60..66 {
        TokenOpenBracket@60..61 "["
        TokenSlash@61..62 "/"
        TokenMod@62..65 "mod"
        TokenCloseBracket@65..66 "]"
      }
    }
    NodeText@66..69 {
      TokenText@66..69 " a "
    }
    NodeModClose@69..75 {
      TokenOpenBracket@69..70 "["
      TokenSlash@70..71 "/"
      TokenMod@71..74 "mod"
      TokenCloseBracket@74..75 "]"
    }
  }
  NodeText@75..86 {
    TokenText@75..86 " a [/mod] a"
  }
}
